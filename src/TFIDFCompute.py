# encoding:utf8


import jieba
import math
import codecs
import sys
import os
import itertools





reload(sys)
sys.setdefaultencoding('utf8')
os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'

class Document():
    doc_id = ''
    word_sum = 0
    word_list = []
    word_key_list = []
    word_count_dic = {}
    word_doccount_dic = {}

    def __init__(self, docid, word_li, wordcount = {}, word_doccount = {}):
        self.doc_id = docid
        self.word_list = word_li
        self.word_sum = len(self.word_list)
        self.word_count_dic = wordcount
        self.word_doccount_dic = word_doccount
        init = [0] * self.word_sum
        temp_dic = dict(itertools.izip(self.word_list, init))
        self.word_key_list = temp_dic.keys()




    def setword_doccount_dic(self, word_doccount):
        self.word_doccount_dic = word_doccount


    def setword_count_dic(self, word_count):
        self.word_count_dic = word_count





class Tfidf():
    word_tfidf_dic = {}




    def get_keyword(self, filename):
        word_list = []
        if filename is not None:
            fi = codecs.open(filename, 'r', 'utf-8')
            line = ' '
            while line:
                line = fi.readline()
                temp_list = list(jieba.cut(line))
                word_list.extend(temp_list)
        return word_list




    def word_count(self, docobj):
        word_list = docobj.word_list
        word_count_dic = {}
        word_num = len(word_list)
        count_init = [0] * word_num
        word_count_dic = dict(itertools.izip(word_list, count_init))
        for word in word_list:
            temp_count = word_count_dic.get(word)
            temp_count += 1
            word_count_dic.update({word: temp_count})
        return word_count_dic



    def word_doccount(self, docobj, doc_set):
        word_doccount_dic = {}
        word_list = docobj.word_key_list
        word_num = len(word_list)
        count_init = [0] * word_num
        word_doccount_dic = dict(itertools.izip(word_list, count_init))
        for word in word_list:
            tem_count = word_doccount_dic.get(word)
            for doc in doc_set:
                if word in doc.word_key_list >= 0:
                    tem_count += 1
            word_doccount_dic.update({word: tem_count})
        return word_doccount_dic



    def tfidf_compute(self, docobj, doc_set):
        word_list = docobj.word_key_list
        word_sum = docobj.word_sum
        doc_sum = len(doc_set)

        word_doccount_dic = self.word_doccount(docobj, doc_set)
        word_count_dic = self.word_count(docobj)

        word_num = len(word_list)
        count_init = [0] * word_num
        self.word_tfidf_dic = dict(itertools.izip(word_list, count_init))
        for word in word_list:
            tf = word_count_dic.get(word) * 1.0/word_sum
            idf = math.log(doc_sum/(1.0 + word_doccount_dic.get(word))) + 1
            word_tfidf = tf * idf
            self.word_tfidf_dic.update({word: word_tfidf})



if __name__ == '__main__':
    doc_set = []    #document object list
    tfidf = Tfidf()
    word_list = tfidf.get_keyword('song-cleaned.txt') #get the single word from the text document
    doc = Document('1', word_list) #initialing one document
    doc_set.append(doc)
    dic = tfidf.word_count(doc) #count the frequency of each word in one document
    doc_dic = tfidf.word_doccount(doc, doc_set) #count the number of documents containing specific key word
    tfidf.tfidf_compute(doc, doc_set) #compute tf-idf for each word in one document
    print tfidf.word_tfidf_dic.values()
